package com.example.mnasatchallenge;

import android.app.Application;
import android.widget.Toast;

import com.example.mnasatchallenge.model.PopularActorDetails;
import com.example.mnasatchallenge.model.PopularPersonsResponse;
import com.example.mnasatchallenge.model.Profile;
import com.example.mnasatchallenge.model.Result;
import com.example.mnasatchallenge.network.ApiEndpointInterface;
import com.example.mnasatchallenge.network.ApiManager;
import com.example.mnasatchallenge.utils.Constants;

import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.mnasatchallenge.network.ApiManager.networkInterface;

public class Repository {


    public MutableLiveData<PopularPersonsResponse> popularPersonsResponseMutableLiveData = new MediatorLiveData<>();
    public MutableLiveData<PopularActorDetails> actorDetailsMutableLiveData = new MediatorLiveData<>();
    MutableLiveData<Profile> profileMutableLiveData = new MediatorLiveData<>();
    MutableLiveData<Result> resultMutableLiveData = new MediatorLiveData<>();


    public void popularPersons(String apiKey, int page) {

        Call<PopularPersonsResponse> call = new ApiManager().networkInterface.popularPersons(Constants.API_KEY, page);
        call.clone().enqueue(new Callback<PopularPersonsResponse>() {
            @Override
            public void onResponse(Call<PopularPersonsResponse> call, Response<PopularPersonsResponse> response) {

                if (response.isSuccessful()) {
                    PopularPersonsResponse popularPersonsResponse = (PopularPersonsResponse) response.body();
                    popularPersonsResponseMutableLiveData.postValue(popularPersonsResponse);
                } else {

                }
            }

            @Override
            public void onFailure(Call<PopularPersonsResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void popularPersonImages(String personId, String apiKey) {

        Call<PopularPersonsResponse> call = new ApiManager().networkInterface.popularPersonImages(personId, Constants.API_KEY);
        call.clone().enqueue(new Callback<PopularPersonsResponse>() {
            @Override
            public void onResponse(Call<PopularPersonsResponse> call, Response<PopularPersonsResponse> response) {

                if (response.isSuccessful()) {
                    PopularPersonsResponse popularImageResponse = (PopularPersonsResponse) response.body();
                    popularPersonsResponseMutableLiveData.postValue(popularImageResponse);


                } else {

                }
            }

            @Override
            public void onFailure(Call<PopularPersonsResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });

    }


    public void popularPersonDetails(String personId, String apiKey) {
        Call<PopularActorDetails> call = new ApiManager().networkInterface.popularPersonDetails(personId, Constants.API_KEY);
        call.clone().enqueue(new Callback<PopularActorDetails>() {
            @Override
            public void onResponse(Call<PopularActorDetails> call, Response<PopularActorDetails> response) {

                if (response.isSuccessful()) {
                    PopularActorDetails popularActorDetails = (PopularActorDetails) response.body();
                    actorDetailsMutableLiveData.postValue(popularActorDetails);

                } else {

                }
            }

            @Override
            public void onFailure(Call<PopularActorDetails> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


    public void searchPersons(String apiKey, int page, String actorName) {

        Call<PopularPersonsResponse> call = networkInterface.searchPersons(apiKey, page, actorName);
        call.clone().enqueue(new Callback<PopularPersonsResponse>() {
            @Override
            public void onResponse(Call<PopularPersonsResponse> call, Response<PopularPersonsResponse> response) {

                if (response.isSuccessful()) {
                    PopularPersonsResponse popularSearchResponse = (PopularPersonsResponse) response.body();
                    popularPersonsResponseMutableLiveData.postValue(popularSearchResponse);
                } else {

                }
            }

            @Override
            public void onFailure(Call<PopularPersonsResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });

    }
}
