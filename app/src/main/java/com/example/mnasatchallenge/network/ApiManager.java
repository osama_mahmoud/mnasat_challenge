package com.example.mnasatchallenge.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {

    public static ApiManager apiManager = null;
    private Retrofit retrofit;
    public static ApiEndpointInterface networkInterface;
    private OkHttpClient.Builder httpClient;
    private Retrofit.Builder builder;
    private static HttpLoggingInterceptor loggingInterceptor;


    public ApiManager() {
        init();
    }

    public static synchronized ApiManager getInstance() {

        if (apiManager == null) {
            apiManager = new ApiManager();
        }
        return apiManager;
    }

    private void init() {
        loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    request = request.newBuilder()
                            .build();
                    return chain.proceed(request);
                })
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .hostnameVerifier((hostname, session) -> true);

        //if(BuildConfig.DEBUG){  // only enable log in debug mode to still secure my requests like password ..
        httpClient.addInterceptor(loggingInterceptor);
        //}

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        builder = new Retrofit.Builder()
                .baseUrl(ApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        retrofit = builder.client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        networkInterface = retrofit.create(ApiEndpointInterface.class);
    }

}
