package com.example.mnasatchallenge.viewmodel;

import android.app.Application;

import com.example.mnasatchallenge.Repository;
import com.example.mnasatchallenge.model.PopularActorDetails;
import com.example.mnasatchallenge.model.PopularPersonsResponse;
import com.example.mnasatchallenge.model.Profile;
import com.example.mnasatchallenge.model.Result;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ActorsViewModel extends ViewModel {

    private Repository repository = new Repository();
    public MutableLiveData<PopularPersonsResponse> popularPersonsResponseMutableLiveData = repository.popularPersonsResponseMutableLiveData;
    public MutableLiveData<PopularActorDetails> actorDetailsMutableLiveData = repository.actorDetailsMutableLiveData;
    MutableLiveData<Profile> profileMutableLiveData;
    MutableLiveData<Result> resultMutableLiveData;


    public void popularPersons(String apiKey, int page) {
        repository.popularPersons(apiKey, page);
    }

    public void popularPersonImages(String personId, String apiKey) {
        repository.popularPersonImages(personId, apiKey);
    }

    public void popularPersonDetails(String personId, String apiKey) {
        repository.popularPersonDetails(personId, apiKey);
    }

    public void searchPersons(String apiKey, int page, String actorName) {
        repository.searchPersons(apiKey, page, actorName);
    }
}
