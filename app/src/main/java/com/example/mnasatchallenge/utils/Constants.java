package com.example.mnasatchallenge.utils;

public class Constants {

    public static final String API_KEY = "2c7cf034b27193d16ea96533ca9923ae";

    public static String POPULAR_ACTOR_ID = "popular_actor_id";
    public static String POPULAR_ACTOR_NAME = "popular_actor_name";
    public static String POPULAR_ACTOR_IMAGE = "popular_actor_image";
    public static String POPULAR_ACTOR_POPULARITY = "popular_actor_popularity";
}
