package com.example.mnasatchallenge.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mnasatchallenge.R;
import com.example.mnasatchallenge.adapter.PopularActorsAdapter;
import com.example.mnasatchallenge.model.Result;
import com.example.mnasatchallenge.utils.Constants;
import com.example.mnasatchallenge.utils.EndlessRecyclerViewScrollListener;
import com.example.mnasatchallenge.viewmodel.ActorsViewModel;
import com.example.mnasatchallenge.views.activity.MainActivity;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;

public class PopularActorsFragment extends Fragment implements PopularActorsAdapter.OnItemClickListener {

    private OnPopularActorsFragmentInteractionListener mListener;

    public PopularActorsFragment() {
        // Required empty public constructor
    }


    public static PopularActorsFragment newInstance() {
        return new PopularActorsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }


    //    @BindView(R.id.main_recyclerview)
    RecyclerView rvPopularActors;
    //  @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private ActorsViewModel actorsViewModel;

    PopularActorsAdapter adapter;
    android.app.AlertDialog dialog;
    int pageNo = 1;
    ArrayList<Result> popularActorsList = new ArrayList<>();
    private EndlessRecyclerViewScrollListener scrollListener;
    int totalPages = 0;
    boolean flag = false;

    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        actorsViewModel = ViewModelProviders.of(this).get(ActorsViewModel.class);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_popular_actors, container, false);

        rvPopularActors = view.findViewById(R.id.main_recyclerview);
        progressBar = view.findViewById(R.id.progressBar);

        unbinder = ButterKnife.bind(this, view);
        initViews();
        actorsViewModel.popularPersonsResponseMutableLiveData.observe(getActivity(), popularPersonsResponse -> {

            if (popularPersonsResponse != null) {
                dialog.dismiss();
                progressBar.setVisibility(View.INVISIBLE);
                popularActorsList.addAll(popularPersonsResponse.getResults());
                totalPages = popularPersonsResponse.getTotalPages();
                adapter.notifyDataSetChanged();
            }
        });


        return view;
    }

    private void initViews() {


        rvPopularActors.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        rvPopularActors.setLayoutManager(gridLayoutManager);
        adapter = new PopularActorsAdapter(getActivity(), popularActorsList);
        adapter.setOnItemClickListener(this);
        rvPopularActors.setAdapter(adapter);
        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                progressBar.setVisibility(View.VISIBLE);
                pageNo++;
                if (pageNo <= totalPages)
                    callPopularPersonsApi();

            }
        };
        rvPopularActors.addOnScrollListener(scrollListener);

        callPopularPersonsApi();
    }


    private void callPopularPersonsApi() {
        dialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .setTheme(R.style.CustomSpotDialog)
                .setCancelable(false)
                .build();


        if (!flag) {
      //      dialog.show();
            actorsViewModel.popularPersons(Constants.API_KEY, pageNo);
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPopularActorsFragmentInteractionListener) {
            mListener = (OnPopularActorsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPopularActorsFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface OnPopularActorsFragmentInteractionListener {
        void onPopularActorsFragmentInteraction(int id, String name, String image, float popularity);
    }



    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.actors));
        }
    }


    @Override
    public void onItemClick(int popularActorId, String name, String image, float popularity) {
        if (getActivity() != null)
            mListener.onPopularActorsFragmentInteraction(popularActorId, name, image, popularity);
    }
}
