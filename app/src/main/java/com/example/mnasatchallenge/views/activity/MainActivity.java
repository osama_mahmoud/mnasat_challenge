package com.example.mnasatchallenge.views.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProviders;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.mnasatchallenge.R;
import com.example.mnasatchallenge.model.PopularPersonsResponse;
import com.example.mnasatchallenge.model.Result;
import com.example.mnasatchallenge.utils.Constants;
import com.example.mnasatchallenge.viewmodel.ActorsViewModel;
import com.example.mnasatchallenge.views.fragment.PopularActorDetailsFragment;
import com.example.mnasatchallenge.views.fragment.PopularActorsFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PopularActorsFragment.OnPopularActorsFragmentInteractionListener
        , PopularActorDetailsFragment.OnPopularActorDetailsFragmentInteractionListener {

    private ActorsViewModel searchViewModel;
    private ArrayList<Result> popularActorsList = new ArrayList<>();
    private PopularPersonsResponse popularPersonsResponse;
    int pageNo = 1;
    int totalPages = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        //     searchViewModel = ViewModelProviders.of(this).get(ActorsViewModel.class);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, new PopularActorsFragment()).commit();

//        searchViewModel.popularPersonsResponseMutableLiveData.observe(this, popularPersonsResponse1 -> {
//
//            if (popularPersonsResponse != null) {
//                popularActorsList.addAll(popularPersonsResponse.getResults());
//                totalPages = popularPersonsResponse.getTotalPages();
//            }
//
//        });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.search_activity_menu, menu);
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        final SearchView searchView = (SearchView) menu.findItem(R.id.ic_search).getActionView();
//        MenuItem searchMenuItem = menu.findItem(R.id.ic_search);
//
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setQueryHint("Search...");
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                if (query.length() > 2) {
//                    searchViewModel.searchPersons(Constants.API_KEY, pageNo, query);
//                }
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                searchViewModel.searchPersons(Constants.API_KEY, pageNo, newText);
//                return false;
//            }
//        });
//        searchMenuItem.getIcon().setVisible(false, false);
//        return true;
//    }


    public void setActionBarTitle(String title) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);
    }

    @Override
    public void onPopularActorsFragmentInteraction(int id, String name, String image, float popularity) {
        getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.container,
                PopularActorDetailsFragment.newInstance(id, name, image, popularity)).commit();
    }

    @Override
    public void onPopularActorDetailsFragmentInteraction() {

    }
}
