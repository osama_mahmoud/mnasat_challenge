package com.example.mnasatchallenge.views.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mnasatchallenge.R;
import com.example.mnasatchallenge.adapter.PopularActorsImagesAdapter;
import com.example.mnasatchallenge.model.Profile;
import com.example.mnasatchallenge.utils.Constants;
import com.example.mnasatchallenge.viewmodel.ActorsViewModel;
import com.example.mnasatchallenge.views.activity.MainActivity;
import com.example.mnasatchallenge.views.activity.SearchActivity;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;

public class PopularActorDetailsFragment extends Fragment {
    private String id;
    private String name;
    private String image;
    private float popularity;

    private OnPopularActorDetailsFragmentInteractionListener mListener;

    public PopularActorDetailsFragment() {
        // Required empty public constructor
    }


    public static PopularActorDetailsFragment newInstance(int popularActorId, String name, String image, float popularity) {
        PopularActorDetailsFragment fragment = new PopularActorDetailsFragment();
        Bundle args = new Bundle();
        args.putString(Constants.POPULAR_ACTOR_ID, String.valueOf(popularActorId));
        args.putString(Constants.POPULAR_ACTOR_NAME, name);
        args.putString(Constants.POPULAR_ACTOR_IMAGE, image);
        args.putString(Constants.POPULAR_ACTOR_POPULARITY, String.valueOf(popularity));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(Constants.POPULAR_ACTOR_ID);
            name = getArguments().getString(Constants.POPULAR_ACTOR_NAME);
            image = getArguments().getString(Constants.POPULAR_ACTOR_IMAGE);
            popularity = Float.parseFloat(getArguments().getString(Constants.POPULAR_ACTOR_POPULARITY));
        }
    }

    private Unbinder unbinder;
    // @BindView(R.id.main_recyclerview)
    RecyclerView rvPopularActors;
    // @BindView(R.id.iv_image)
    RoundedImageView ivPopularActorImage;
    //   @BindView(R.id.tv_name)
    TextView tvActorName;
    //  @BindView(R.id.tvInfo)
    TextView tvInfo;
    //   @BindView(R.id.tv_Rating)
    TextView tvActorPopularity;
    //    @BindView(R.id.tvBirthDate)
    TextView tvBirthDate;
    private ActorsViewModel detailViewModel;
    /*@BindView(R.id.progressBar)
    ProgressBar progressBar;
*/
    PopularActorsImagesAdapter adapter;
    android.app.AlertDialog dialog;
    ArrayList<Profile> popularActorsImagesList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        detailViewModel = ViewModelProviders.of(this).get(ActorsViewModel.class);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_popular_actor_details, container, false);
        unbinder = ButterKnife.bind(this, view);

        rvPopularActors = view.findViewById(R.id.main_recyclerview);
        ivPopularActorImage = view.findViewById(R.id.iv_image);
        tvActorName = view.findViewById(R.id.tv_name);
        tvInfo = view.findViewById(R.id.tvInfo);
        tvActorPopularity = view.findViewById(R.id.tv_Rating);
        tvBirthDate = view.findViewById(R.id.tvBirthDate);

        dialog = new SpotsDialog.Builder().setContext(getActivity()).setTheme(R.style.CustomSpotDialog).setCancelable(false).build();

        if (image != null && !image.isEmpty() && getActivity() != null) {
            Picasso.get().load(image).fit().placeholder(R.drawable.dark_img).into(ivPopularActorImage);
        } else {
            Picasso.get().load(R.drawable.dark_img).fit().placeholder(R.drawable.dark_img).into(ivPopularActorImage);
        }
        tvActorName.setText(name);
        tvActorPopularity.setText(String.format("%s", popularity));

        initViews();

        detailViewModel.actorDetailsMutableLiveData.observe(getActivity(), popularActorDetails -> {
            if (popularActorDetails != null) {
                tvInfo.setText(popularActorDetails.getBiography());
                tvBirthDate.setText(popularActorDetails.getBirthday());
            }
            detailViewModel.popularPersonsResponseMutableLiveData.observe(getActivity(), popularPersonsResponse -> {
                if (popularPersonsResponse != null) {
                    popularActorsImagesList.addAll(popularPersonsResponse.getProfiles());
                    adapter.notifyDataSetChanged();
                }

            });

        });

        return view;
    }


    private void initViews() {

        rvPopularActors.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        rvPopularActors.setLayoutManager(gridLayoutManager);
        adapter = new PopularActorsImagesAdapter(getActivity(), popularActorsImagesList);
        rvPopularActors.setAdapter(adapter);

        callPopularPersonsImagesApi();
        callPopularActorDetailsApi();
    }

    private void callPopularActorDetailsApi() {

        dialog.dismiss();
        detailViewModel.popularPersonDetails(id, Constants.API_KEY);
    }

    private void callPopularPersonsImagesApi() {

        dialog.dismiss();
        detailViewModel.popularPersonImages(id, Constants.API_KEY);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onPopularActorDetailsFragmentInteraction();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.details));

        } else if (getActivity() != null && getActivity() instanceof SearchActivity) {
            ((SearchActivity) getActivity()).setActionBarTitle(getString(R.string.details));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPopularActorDetailsFragmentInteractionListener) {
            mListener = (OnPopularActorDetailsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public interface OnPopularActorDetailsFragmentInteractionListener {
        // TODO: Update argument type and name
        void onPopularActorDetailsFragmentInteraction();
    }
}
